package com.krd.mini_project1.Helper;

import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.krd.mini_project1.models.Enitiy.Pagination;

public abstract class PaginationHelper extends RecyclerView.OnScrollListener {

    public final static long PAGE_NUM=1;
    public final static long PAGE_LIMIT=15;
    private LinearLayoutManager layoutManager;
    public PaginationHelper( LinearLayoutManager layoutManager){
        this.layoutManager= layoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
//        int itemCount = layoutManager.getItemCount();
//        int visibleItemCount = layoutManager.getChildCount();
//        int firstVisibleItemCount = layoutManager.findFirstVisibleItemPosition();
//        if ((visibleItemCount + firstVisibleItemCount)>= itemCount && firstVisibleItemCount>= 0 && itemCount>=PAGE_LIMIT){
//            loadMoreData();
//        }

    }
    protected abstract void loadMoreData();
}
