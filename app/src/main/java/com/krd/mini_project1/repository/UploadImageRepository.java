package com.krd.mini_project1.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.krd.mini_project1.models.reponse.ImagRespons;
import com.krd.mini_project1.models.service.APIService;
import com.krd.mini_project1.models.service.RetroInstance;
import com.krd.mini_project1.models.service.UploadImageService;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadImageRepository {
    private static final String TAG = UploadImageRepository.class.getSimpleName();
    UploadImageService uploadImageService;

    public UploadImageRepository(){
        uploadImageService = RetroInstance.CreateService(UploadImageService.class);
    }
    //implement insert image

    public MutableLiveData<ImagRespons> uploadImage(File file) {
        final MutableLiveData<ImagRespons> insertImageData = new MutableLiveData<>();

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part image =
                MultipartBody.Part.createFormData("file", file.getAbsolutePath(), requestFile);

        Call<ImagRespons> uploadImageCall = uploadImageService.uploadFile(ArticleRepository.auth,image);

        uploadImageCall.enqueue(new Callback<ImagRespons>() {
            @Override
            public void onResponse(Call<ImagRespons> call, Response<ImagRespons> response) {
                if (response.isSuccessful() && response.body() != null) {
                    insertImageData.postValue(response.body());
                    Log.d(TAG, "uploadImage: " + response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ImagRespons> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
        return insertImageData;
    }
}
