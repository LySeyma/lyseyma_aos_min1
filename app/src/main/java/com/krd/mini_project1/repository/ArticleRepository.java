package com.krd.mini_project1.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.krd.mini_project1.models.Enitiy.Crud_Article;
import com.krd.mini_project1.models.Enitiy.Pagination;
import com.krd.mini_project1.models.reponse.ArticleListResponse;
import com.krd.mini_project1.models.reponse.GetArticleIdResponce;
import com.krd.mini_project1.models.reponse.ImagRespons;
import com.krd.mini_project1.models.reponse.SingleArticleResponse;
import com.krd.mini_project1.models.service.APIService;
import com.krd.mini_project1.models.service.RetroInstance;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

     APIService apiService ;

    MultipartBody.Builder builder = new MultipartBody.Builder();
//    builder.setType(MultipartBody.FORM);


    public ArticleRepository(){
        apiService = RetroInstance.CreateService(APIService.class);
    }
    public final static String auth = "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=";
    public MutableLiveData<ArticleListResponse> getListArticleByPagination(Pagination pagination) {
        final MutableLiveData<ArticleListResponse> liveData = new MutableLiveData<>();
        Call<ArticleListResponse> call = apiService.getAll(auth,pagination.getPage(), pagination.getLimit());
        call.enqueue(new Callback<ArticleListResponse>() {
            @Override
            public void onResponse(Call<ArticleListResponse> call, Response<ArticleListResponse> response) {

                if (response.isSuccessful()) {

                    Log.d("onResponse: ", response.body().getMessage());
                    liveData.postValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<ArticleListResponse> call, Throwable t) {
                Log.e("onFailure: ", t.getMessage());

            }
        });

        return liveData;

    }

    //implement logic to add article

    public MutableLiveData<SingleArticleResponse> insertArticle(Crud_Article crud_article){
           final MutableLiveData<SingleArticleResponse> insetLiveArticle = new MutableLiveData<>();
        final Call<SingleArticleResponse> insetArticle = apiService.insetArticle(auth,crud_article);
        insetArticle.enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if(response.isSuccessful() && response.body() != null){
                    Log.d("TAG succeed", response.body().getMessage());
                    Log.d("TAG succeed", response.body().toString());
                    insetLiveArticle.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.d("TAG", "onFailure: "+t.getMessage());
            }
        });
//        apiService.insetArticle(auth,crud_article).enqueue(new Callback<SingleArticleResponse>() {
//            @Override
//            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
//                if (response.isSuccessful()){
//                    Log.d("TAG succeed", response.body().getMessage());
//                    Log.d("TAG succeed", response.body().toString());
//                    liveData.postValue(response.body());
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
//                Log.e("tag error: ",t.getMessage()  );
//
//            }
//        });
        return insetLiveArticle;
    }


    //implement update article

    public MutableLiveData<SingleArticleResponse> updateArticle(int id,Crud_Article article){
        final MutableLiveData<SingleArticleResponse> liveData = new MutableLiveData<>();
        apiService.updateArticle(auth,id,article).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()){
                    Log.d(" Tag update succeed", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.e("tag error update: ",t.getMessage()  );

            }
        });
        return liveData;
    }

    //implement getById article

    public MutableLiveData<GetArticleIdResponce> getArticleById(int id){
        final MutableLiveData<GetArticleIdResponce> liveData = new MutableLiveData<>();
        Call<GetArticleIdResponce> call = apiService.getOne(auth,id);
        call.enqueue(new Callback<GetArticleIdResponce>() {
            @Override
            public void onResponse(Call<GetArticleIdResponce> call, Response<GetArticleIdResponce> response) {

                if (response.isSuccessful()) {

                    Log.d("onResponse: ", response.body().getMessage());
                    liveData.postValue(response.body());

                }
            }

            @Override
            public void onFailure(Call<GetArticleIdResponce> call, Throwable t) {
                Log.e("onFailure: ", t.getMessage());

            }
        });
        return liveData;
    }


    //implement delete article

   public MutableLiveData<SingleArticleResponse> deleteArticleById(int id){
        final MutableLiveData<SingleArticleResponse> liveData = new MutableLiveData<>();
        apiService.deleteArticle(auth,id).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful()){
                    Log.d(" Tag Delete succeed", response.body().getMessage());
                    liveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.e("tag error delete: ",t.getMessage()  );
            }
        });
        return liveData;
    }



}
