package com.krd.mini_project1.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.krd.mini_project1.R;
import com.krd.mini_project1.models.Enitiy.Article;
import com.krd.mini_project1.models.Enitiy.Crud_Article;
import com.krd.mini_project1.models.reponse.ArticleListResponse;
import com.krd.mini_project1.models.reponse.GetArticleIdResponce;
import com.krd.mini_project1.models.reponse.ImagRespons;
import com.krd.mini_project1.models.reponse.SingleArticleResponse;
import com.krd.mini_project1.viewmodel.AddArticleViewModel;
import com.krd.mini_project1.viewmodel.ArticleListViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class detail_acticle extends AppCompatActivity {

    public ImageView img_addArticle;
    public TextView tv_title,tv_desc,tv_cate;
    private int articleId;
    private AddArticleViewModel addArticleViewModel ;
    private ArticleListViewModel articleListResponse;
    private Article article;
    private ImagRespons imagRespons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_acticle);
        onNewIntent(getIntent());
        img_addArticle= findViewById(R.id.img_Article);
        tv_title = findViewById(R.id.tv_title);
        tv_desc = findViewById(R.id.tv_desc);
        tv_cate =  findViewById(R.id.tv_cate);

        addArticleViewModel = ViewModelProviders.of(this).get(AddArticleViewModel.class);
        addArticleViewModel.setId(articleId);
        addArticleViewModel.init();
        addArticleViewModel.getTriggerData().observe(this, new Observer<GetArticleIdResponce>() {
            @Override
            public void onChanged(GetArticleIdResponce articleListResponse) {
                Log.d(" tag Change",articleListResponse + " ");
                article = new Article();
                article = articleListResponse.getData();
                getData();
            }
        });


        addArticleViewModel.getArticleByID();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle extras = intent.getExtras();
        if (extras != null){
            articleId = extras.getInt("id");

        }

    }

    private void getData(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date=null;
        try {
            date = formatter.parse(article.getCreated_date());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        formatter = new SimpleDateFormat("dd MMM yyyy HH:mm");
        tv_title.setText(article.getTitle());
        tv_cate.setText(formatter.format(date));
        tv_desc.setText(article.getDescription());
                Glide.with(this)
                .load(article.getImageUrl())
                .centerCrop()
                .placeholder(R.drawable.ass)
                .into(img_addArticle);
    }
}