package com.krd.mini_project1.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileUtils;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.krd.mini_project1.R;
import com.krd.mini_project1.models.Enitiy.Article;
import com.krd.mini_project1.models.Enitiy.Crud_Article;
import com.krd.mini_project1.models.reponse.ArticleListResponse;
import com.krd.mini_project1.models.reponse.GetArticleIdResponce;
import com.krd.mini_project1.models.reponse.ImagRespons;
import com.krd.mini_project1.models.reponse.SingleArticleResponse;
import com.krd.mini_project1.models.service.APIService;
import com.krd.mini_project1.models.service.RetroInstance;
import com.krd.mini_project1.repository.ArticleRepository;
import com.krd.mini_project1.viewmodel.AddArticleViewModel;
import com.krd.mini_project1.viewmodel.AddImageViewModel;
import com.krd.mini_project1.viewmodel.ArticleListViewModel;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import dalvik.system.DexFile;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.Multipart;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AddArticleActivity extends AppCompatActivity implements View.OnClickListener {

    public ImageView img_addArticle;
    public EditText ed_title,ed_desc,ed_cate;
    public Button btn_save,btn_cancel;
    private AddArticleViewModel addArticleViewModel;
    private ArticleListViewModel articleListResponse;
    private Crud_Article crud_article;
    private int articleId;
    private Bitmap bitmap;
    private static final int PICK_IMAGE_CODE = 101;
    private static final int PERMISSION_CODE = 1000;

    //Uri to store the image uri
    private Uri filePath;
    private AddImageViewModel addImageViewModel;
    private String imageUrl;
    private Article article;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);
        img_addArticle= findViewById(R.id.img_addArticle);
        ed_title = findViewById(R.id.ed_title);
        ed_desc = findViewById(R.id.ed_desc);
        ed_cate =  findViewById(R.id.ed_cate);
        btn_cancel= findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);
        btn_save= findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        img_addArticle.setOnClickListener(this);
        addImageViewModel = ViewModelProviders.of(this).get(AddImageViewModel.class);
        addImageViewModel.init();
        addArticleViewModel = ViewModelProviders.of(this).get(AddArticleViewModel.class);
        addArticleViewModel.init();
        addArticleViewModel.getTriggerData().observe(this, new Observer<GetArticleIdResponce>() {
            @Override
            public void onChanged(GetArticleIdResponce articleListResponse) {
                Log.d(" tag Change",articleListResponse + " ");
                article = new Article();
                article= articleListResponse.getData();
//                setUpArticleForUpdate();
            }
        });
        addArticleViewModel.getArticleByID();
        requestStoragePermission();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_save){
            addArticleViewModel.insertArticleLiveData(getData()).observe(this, new Observer<SingleArticleResponse>() {
                @Override
                public void onChanged(SingleArticleResponse singleArticleResponse) {
//                    Log.d(" tag image", imageUrl);
                    Log.d(TAG, "onChanged: "+singleArticleResponse);
                }
            });
            finish();
        }else if (v.getId() == R.id.btn_cancel){
            finish();

        }else if (v.getId() == R.id.img_addArticle){
            showFileChooser();

        }

    }
    //method to show file chooser
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_CODE);
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            Glide.with(this).load(filePath).centerCrop().into(img_addArticle);
            String path = getPath(filePath);
            Uri uri = Uri.parse(path);
            File file = new File(uri.getPath());
//            Log.d("TAG", String.valueOf(file));
            addImageViewModel.uploadImageLiveData(file).observe(this, new Observer<ImagRespons>() {
                @Override
                public void onChanged(ImagRespons imagRespons) {
                    imageUrl = imagRespons.getData();
                    Log.d(" tag path", imageUrl);
                }
            });
            addImageViewModel.uploadImageLiveData(file).observe(this, new Observer<ImagRespons>() {
                @Override
                public void onChanged(ImagRespons imagRespons) {
                    imageUrl = imagRespons.getData();
//                    uploadProgressbar.setVisibility(View.GONE);
                    Log.d(TAG, "onActivityResult: "+imageUrl);
                }
            });

        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        Log.d(" tag path", path);
        cursor.close();

        return path;
    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE);
    }
    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }


    private Crud_Article getData() {
        final String title = ed_title.getText().toString().trim();
        final String cate = ed_cate.getText().toString().trim();
        final String desc = ed_desc.getText().toString().trim();

        Crud_Article crud_article = new Crud_Article();
        crud_article.setTitle(title);
        crud_article.setCreated_date(cate);
        crud_article.setDescription(desc);
        crud_article.setImageUrl(imageUrl);

        return crud_article;
    }

}