package com.krd.mini_project1.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.krd.mini_project1.Helper.MySwipeHelper;
import com.krd.mini_project1.R;
import com.krd.mini_project1.models.Enitiy.Article;
import com.krd.mini_project1.models.Enitiy.Crud_Article;
import com.krd.mini_project1.models.Enitiy.Pagination;
import com.krd.mini_project1.models.reponse.ArticleListResponse;
import com.krd.mini_project1.models.reponse.ImagRespons;
import com.krd.mini_project1.models.service.OnItemViewClickListener;
import com.krd.mini_project1.viewmodel.AddArticleViewModel;
import com.krd.mini_project1.viewmodel.ArticleListViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ArticleListResponse datSet;
    private ImagRespons imagRespons;
    private RecyclerView recyclerView;
    private ArticleListAdapter adapter;
    private ArticleListViewModel viewModel;
    private SwipeRefreshLayout swipeContainer;
    private AddArticleViewModel addArticleViewModel;
    public static final int  ADD_NOTE_REQUEST = 1;
    Context context;
    private boolean isReload = false;
    private boolean isUpdate = false;
    private int updateItemPosition = 0;
    private int articleId = 0;
    private FloatingActionButton fab;
    public LinearLayoutManager layoutManager;
    private long currentPage = 1,limit = 15, totalPage;
    private ArticleListResponse articleListResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.rv_list);
        fab =findViewById(R.id.btn_fba);
        setupSwipeRefreshLayout();
        setupPagination();
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
            @Override public void onItemClick(View view, int position) {
                // do whatever\
                int id = datSet.getData().get(position).getId();
                Intent detailIntent = new Intent(
                        MainActivity.this,detail_acticle.class
                );
                detailIntent.putExtra("id",id);
                Log.d(" tag", String.valueOf(id));
                startActivity(detailIntent);
            }


            @Override public void onLongItemClick(View view, int position) {
                // do whatever
//                Toast.makeText(context," onclick lo",Toast.LENGTH_LONG).show();
            }
        }));


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && fab.isShown())
                    fab.hide();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                    fab.show();
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        context = this;
        viewModel = ViewModelProviders.of(this).get(ArticleListViewModel.class);
        viewModel.init();
        getLiveDataFromViewModel();
//        fetchListArticle();
        addArticle();
    }
    private void swiper(){
        MySwipeHelper mySwipeHelper = new MySwipeHelper(this,recyclerView,200) {

            @Override
            public void instantiateUnderlayButton(final RecyclerView.ViewHolder viewHolder, List<MySwipeHelper.MyButton> buffer) {
                buffer.add(new MyButton(MainActivity.this,
                        "Delete",
                        50,
                        R.drawable.delete,
                        Color.parseColor("#aa3a3a"),
                        new MyButtonClickListener(){
                            @Override
                            public void onClick(final int pos) {


                                final Article article = adapter.getData().get(pos);
                                articleId = datSet.getData().get(pos).getId();
                                viewModel.setId(articleId);
                                viewModel.deleteArticleById();
                                adapter.notifyItemRemoved(pos);
                                Snackbar snackbar = Snackbar.make(recyclerView, "Item was removed from the list.", Snackbar.LENGTH_LONG);
                                snackbar.setAction("UNDO", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        adapter.restoreItem(article, pos);
                                        recyclerView.scrollToPosition(pos);
                                    }
                                });

                                snackbar.setActionTextColor(Color.YELLOW);
                                snackbar.show();

                            }
                        }
                ));
                buffer.add(new MyButton(MainActivity.this,
                        "Update",
                        30,
                        R.drawable.ic_baseline_edit_24,
                        Color.parseColor("#a4b787"),
                        new MyButtonClickListener(){
                            @Override
                            public void onClick(int pos) {
                                isUpdate = true;
                                updateItemPosition = pos;
                                int id = datSet.getData().get(pos).getId();
                                Intent updateIntent = new Intent(
                                        MainActivity.this,AddArticleActivity.class
                                );
                                updateIntent.putExtra("id",id);
                                Log.d(" tag", String.valueOf(id));
                                startActivity(updateIntent);
                            }
                        }
                ));

            }
        };
        mySwipeHelper.attachToRecyclerView(recyclerView);
    }

    private void setupRecycleView(){

        if (adapter == null){
            layoutManager = new LinearLayoutManager(this);
            adapter = new ArticleListAdapter(this,datSet.getData());
//            adapter = new ArticleListAdapter(this, new ArticleListAdapter(), new OnItemViewClickListener() {
//                @Override
//                public void onItemViewClick(View view, Crud_Article article) {
//                    Intent intent = new Intent(this, detail_acticle.class);
//                    intent.putExtra("DATA",Crud_Article);
//                    startActivity(intent);
//                }
//            });
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
            swiper();

        }else if (isReload){
//            viewModel.setId(articleId);
            adapter.appendDataSet(datSet.getData());
//            adapter.setDataSet(updateItemPosition,viewModel.getArticleById());
            adapter.notifyDataSetChanged();
            isReload= false;
        }
        else {
            adapter.appendDataSet(datSet.getData());
            adapter.notifyDataSetChanged();
        }

        recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                int count = layoutManager.getItemCount();
                int visibleItem = layoutManager.getChildCount();
                Log.d(" tag scroll"," count"+ " -"+ visibleItem);
            }
        });
        // TODO: implement recycler view pagination
//        recyclerView.addOnScrollListener(new PaginationHelper(layoutManager) {
//            @Override
//            protected void loadMoreData() {
////                viewModel.get
//
//            }
//        });


    }

    private void setupPagination() {
        isReload = true;
        currentPage = 1;
//        Paginate.with(recyclerViewArticles,callbacks)
//                .addLoadingListItem(true)
//                .setLoadingTriggerThreshold(2)
//                .setLoadingListItemSpanSizeLookup(()-> 2)
//                .build();
//        callbacks.onLoadMore();
    }
    private void setupSwipeRefreshLayout(){
        swipeContainer = findViewById(R.id.swipe_container);
        swipeContainer.setOnRefreshListener(()->{
            totalPage = 0;
            currentPage = 1;
//            fetchListArticle();
            adapter.clear();
            swipeContainer.setRefreshing(false);
        });
    }
    private void getLiveDataFromViewModel(){
        viewModel.getLiveData().observe(this, new Observer<ArticleListResponse>() {
            @Override
            public void onChanged(ArticleListResponse articleListResponse) {
                datSet = articleListResponse;
                Log.d("TAG", "onChanged: ");
                setupRecycleView();
            }
        });

    }
    private void addArticle(){
        FloatingActionButton floatingActionButton = findViewById(R.id.btn_fba);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddArticleActivity.class);
                startActivityForResult(intent,ADD_NOTE_REQUEST);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isUpdate){
            viewModel.setId(articleId);
//            adapter.setDataSet(updateItemPosition,viewModel.getArticleById());
            adapter.notifyItemChanged(updateItemPosition);
            updateItemPosition = 0;
        }
        else {
            isReload = true;
//            viewModel.getPagination().setPage(1);
            getLiveDataFromViewModel();
        }
    }
}