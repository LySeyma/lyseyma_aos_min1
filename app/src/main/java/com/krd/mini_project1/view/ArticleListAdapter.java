package com.krd.mini_project1.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.krd.mini_project1.R;
import com.krd.mini_project1.models.Enitiy.Article;
import com.krd.mini_project1.models.Enitiy.Crud_Article;
import com.krd.mini_project1.models.reponse.ArticleListResponse;
import com.krd.mini_project1.models.reponse.ImagRespons;
import com.krd.mini_project1.models.reponse.SingleArticleResponse;
import com.krd.mini_project1.models.service.OnItemViewClickListener;
import com.krd.mini_project1.viewmodel.AddArticleViewModel;
import com.krd.mini_project1.viewmodel.ArticleListViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ArticleViewHolder> {
    private Context context;
    List<Article> dataSet;
    private Crud_Article crud_article;
    ImagRespons imagRespons;
    ArticleListResponse articleListResponse;
//    OnItemViewClickListener onItemViewClickListener;
    public ArticleListAdapter(Context context, List<Article> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
//        this.onItemViewClickListener = onItemViewClickListener;
    }
    public List<Article> getData() {
        return dataSet;
    }

//    public void setDataSet(int position, Article crud_article) {
//        this.dataSet.add(position,crud_article);
//    }

    public void setDataSet(ArticleListResponse articleListResponse) {
        this.articleListResponse = articleListResponse;
        this.articleListResponse.getData().clear();
        notifyDataSetChanged();
    }
    public void clear() {
        if(articleListResponse.getData() != null){
            this.articleListResponse.getData().clear();
            notifyDataSetChanged();
        }
    }

    public void appendDataSet(List<Article> dataSet){
        this.dataSet.addAll(dataSet);
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item,parent,false);
        return new ArticleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {

        Glide.with(context).load(dataSet.get(position).getImageUrl()).placeholder(R.drawable.preview).into(holder.img_article);
        holder.tv_article.setText(dataSet.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        if (dataSet == null){
            return 0;
        }
        else {
            return dataSet.size();
        }
    }
    public void removeItem(int position) {
        dataSet.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Article article, int position) {
        dataSet.add(position, article);
        notifyItemInserted(position);
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder {
        ImageView img_article;
        TextView tv_article;
        LinearLayout layoutManager;
        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            img_article = itemView.findViewById(R.id.img_article);
            tv_article = itemView.findViewById(R.id.tv_title);
            layoutManager = itemView.findViewById(R.id.myRelative);
        }
    }

  }