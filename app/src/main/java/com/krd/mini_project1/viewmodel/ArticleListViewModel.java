package com.krd.mini_project1.viewmodel;

import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.krd.mini_project1.models.Enitiy.Article;
import com.krd.mini_project1.models.Enitiy.Crud_Article;
import com.krd.mini_project1.models.Enitiy.Pagination;
import com.krd.mini_project1.models.reponse.ArticleListResponse;
import com.krd.mini_project1.repository.ArticleRepository;

public class ArticleListViewModel extends ViewModel {

//    private MutableLiveData<ArticleListResponse> triggerData = new MutableLiveData<>();
//    private LiveData<ArticleListResponse> liveData;
private MutableLiveData<ArticleListResponse> triggerData;

    private ArticleRepository articleRepository;
    private Pagination pagination;
    private int id;

    public void init(){
        articleRepository = new ArticleRepository();
        pagination = new Pagination();

        triggerData= articleRepository.getListArticleByPagination(pagination);

    }
    public MutableLiveData<ArticleListResponse> getLiveData() {
        return triggerData;
    }



    public Pagination getPagination(){
        return pagination;
    }
//    public void fetchListArticleByPaging(){
////        triggerData.setValue(articleRepository.getListArticleByPagination(pagination).getValue());
//    }
public MutableLiveData<ArticleListResponse> fetchListArticleByPaging(Pagination pagination){
    return articleRepository.getListArticleByPagination(pagination);
}
    public void deleteArticleById(){
        articleRepository.deleteArticleById(id);
    }
    public Article getArticleById(){
      return (Article) articleRepository.getArticleById(id).getValue().getData();
    }

    public void setId ( int id){
        this.id = id;
    }


}
