package com.krd.mini_project1.viewmodel;

import android.util.Log;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.krd.mini_project1.models.reponse.ImagRespons;
import com.krd.mini_project1.models.reponse.SingleArticleResponse;
import com.krd.mini_project1.repository.ArticleRepository;
import com.krd.mini_project1.repository.UploadImageRepository;

import java.io.File;

public class AddImageViewModel extends ViewModel {

    private UploadImageRepository uploadImage;

    public void init(){
        uploadImage = new UploadImageRepository();
    }
    public MutableLiveData<ImagRespons> uploadImageLiveData(File file){
        return uploadImage.uploadImage(file);
    }
}
