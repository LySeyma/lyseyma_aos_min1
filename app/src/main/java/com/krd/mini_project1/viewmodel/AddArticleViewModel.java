package com.krd.mini_project1.viewmodel;

import android.util.Log;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.krd.mini_project1.models.Enitiy.Crud_Article;
import com.krd.mini_project1.models.reponse.ArticleListResponse;
import com.krd.mini_project1.models.reponse.GetArticleIdResponce;
import com.krd.mini_project1.models.reponse.SingleArticleResponse;
import com.krd.mini_project1.repository.ArticleRepository;

public class AddArticleViewModel extends ViewModel {

    private MutableLiveData<GetArticleIdResponce> triggerData;
    private LiveData<GetArticleIdResponce> liveData;
    private ArticleRepository  articleRepository;
    private int id;
    private Crud_Article article;

    public void init(){
        articleRepository = new ArticleRepository();
        triggerData = new MutableLiveData<>();
        liveData = Transformations.switchMap(triggerData, new Function<GetArticleIdResponce, LiveData<GetArticleIdResponce>>() {
            @Override
            public LiveData<GetArticleIdResponce> apply(GetArticleIdResponce input) {
                return articleRepository.getArticleById(id);
            }

            });
    }

//    public void insertArticle(Crud_Article crud_article){
//
//     triggerData = articleRepository.insertArticle(crud_article);
//
//    }
    public MutableLiveData<SingleArticleResponse> insertArticleLiveData(Crud_Article crud_article){
        return articleRepository.insertArticle(crud_article);
    }

//    public MutableLiveData<SingleArticleResponse> getTriggerData(){
//        return triggerData;
//
//    }
    public LiveData<GetArticleIdResponce> getTriggerData(){
        return liveData;

    }
    public void getArticleByID(){
        triggerData.setValue(articleRepository.getArticleById(id).getValue());

    }
    public void updateArticleById(){
        articleRepository.updateArticle(id,article);
    }
    public void setId(int id){
        this.id= id;
    }

    public void setArticle(Crud_Article article) {
        this.article = article;
    }
}
