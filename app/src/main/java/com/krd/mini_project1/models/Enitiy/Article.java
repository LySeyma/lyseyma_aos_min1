package com.krd.mini_project1.models.Enitiy;

import com.google.gson.annotations.SerializedName;

public class Article {
    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("title")
    private String title;
    @SerializedName("id")
    private int id;
    @SerializedName("created_date")
    private String created_date;
    @SerializedName("description")
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public Article(){

    }
    public Article(String imageUrl, String title) {
        this.imageUrl = imageUrl;
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Article{" +
                "imageUrl='" + imageUrl + '\'' +
                ", title='" + title + '\'' +
                ", id=" + id +
                '}';
    }
}
