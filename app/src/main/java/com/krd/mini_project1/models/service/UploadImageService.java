package com.krd.mini_project1.models.service;

import com.krd.mini_project1.models.reponse.ImagRespons;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadImageService {

    @Multipart
    @POST("/v1/api/uploadfile/single")
    Call<ImagRespons> uploadFile(@Header("Authorization") String auth, @Part MultipartBody.Part image);
}
