package com.krd.mini_project1.models.reponse;

import com.google.gson.annotations.SerializedName;

public class ImagRespons {

    @SerializedName("data")
    private String data;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
