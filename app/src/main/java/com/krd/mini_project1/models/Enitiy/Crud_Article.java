package com.krd.mini_project1.models.Enitiy;

import com.google.gson.annotations.SerializedName;

public class Crud_Article {
    @SerializedName("image")
    private String imageUrl;
    @SerializedName("description")
    private String description;
    @SerializedName("title")
    private String title;
    @SerializedName("created_date")
    private String created_date;
    @SerializedName("id")
    private int id;
    public Crud_Article(){

    }

    public Crud_Article(String imageUrl, String description, String title, String created_date) {
        this.imageUrl = imageUrl;
        this.description = description;
        this.title = title;
        this.created_date = created_date;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Crud_Article{" +
                "imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", created_date='" + created_date + '\'' +
                ", id=" + id +
                '}';
    }
}
