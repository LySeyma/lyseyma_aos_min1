package com.krd.mini_project1.models.reponse;

import com.google.gson.annotations.SerializedName;
import com.krd.mini_project1.models.Enitiy.Article;
import com.krd.mini_project1.models.Enitiy.Crud_Article;

import java.util.List;

public class SingleArticleResponse {

    @SerializedName("data")
    private Crud_Article data;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;
    public SingleArticleResponse(){

    }

    public Crud_Article getData() {
        return data;
    }

    public void setData(Crud_Article data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "SingleArticleResponse{" +
                "data=" + data +
                ", message='" + message + '\'' +
                ", code=" + code +
                '}';
    }
}
