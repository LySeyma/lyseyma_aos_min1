package com.krd.mini_project1.models.reponse;

import com.google.gson.annotations.SerializedName;
import com.krd.mini_project1.models.Enitiy.Article;
import com.krd.mini_project1.models.Enitiy.Pagination;

import java.util.List;

public class GetArticleIdResponce {

    @SerializedName("pagination")
    private Pagination pagination;
    @SerializedName("data")
    private Article data;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public Article getData() {
        return data;
    }

    public void setData(Article data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ArticleListResponse{" +
                "pagination=" + pagination +
                ", data=" + data +
                ", message='" + message + '\'' +
                ", code=" + code +
                '}';
    }
}
