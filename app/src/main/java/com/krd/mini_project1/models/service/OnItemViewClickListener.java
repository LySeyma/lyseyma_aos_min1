package com.krd.mini_project1.models.service;
import com.krd.mini_project1.models.Enitiy.Article;
import com.krd.mini_project1.models.Enitiy.Crud_Article;
import android.view.View;



public interface OnItemViewClickListener {
    void onItemViewClick(View view, Article article);
}
