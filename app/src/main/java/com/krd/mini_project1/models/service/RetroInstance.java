package com.krd.mini_project1.models.service;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroInstance {
    private static Retrofit retro = null;
    public final static String BASE_URL = "http://110.74.194.124:15011/";
    public final static String BASE_UPLOAD_URL = " http://110.74.194.124:15011/image-thumbnails/thumbnail-21b687e3-2186-46c7-838c-e6554b7c5cad.jpeg";
        private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BASIC);
        @NonNull
    private static OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100,TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(new BasicInterceptorHelper("AMSAPIADMIN","AMSAPIP@SSWORD"))
                .build();
    private static Retrofit getClient(){
        if (retro == null){
            retro  = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retro;
    }
        public static <S> S CreateService(Class<S> serviceClass){
        return getClient().create(serviceClass);
    }

//    private static Retrofit getImage(){
//        if (retro == null){
//            retro  = new Retrofit.Builder()
//                    .baseUrl(BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//        }
//        return retro;
//    }
//
//    public static UploadImageService getApiService(){
//            return getImage().create(UploadImageService.class);
//    }
}
