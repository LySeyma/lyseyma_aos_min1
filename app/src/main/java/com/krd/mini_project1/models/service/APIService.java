package com.krd.mini_project1.models.service;

import androidx.lifecycle.MutableLiveData;

import com.krd.mini_project1.models.Enitiy.Article;
import com.krd.mini_project1.models.Enitiy.Crud_Article;
import com.krd.mini_project1.models.reponse.ArticleListResponse;
import com.krd.mini_project1.models.reponse.GetArticleIdResponce;
import com.krd.mini_project1.models.reponse.ImagRespons;
import com.krd.mini_project1.models.reponse.SingleArticleResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {
//    @Headers({"Authorization", "Bearer "+ token})
    @GET("/v1/api/articles")
    Call<ArticleListResponse> getAll(@Header("Authorization") String auth, @Query("page") long page, @Query("limit") long limit);

    @DELETE("/v1/api/articles/{id}")
    Call<SingleArticleResponse> deleteArticle(@Header("Authorization") String auth, @Path("id") int id);

    @PUT("/v1/api/articles/{id}")
    Call<SingleArticleResponse> updateArticle(@Header("Authorization") String auth, @Path("id") int id,@Body Crud_Article crud_article);


    @POST("/v1/api/articles")
    Call<SingleArticleResponse> insetArticle(@Header("Authorization") String auth, @Body Crud_Article crud_article);

    @GET("/v1/api/articles/{id}")
    Call<GetArticleIdResponce> getOne(@Header("Authorization") String auth, @Path("id") int id);
    @Multipart
    @POST("/v1/api/uploadfile/single")
    Call<ImagRespons> uploadFile(@Header("Authorization") String auth, @Part MultipartBody.Part file);

    }
