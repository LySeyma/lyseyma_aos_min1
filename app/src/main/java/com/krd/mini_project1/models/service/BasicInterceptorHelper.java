package com.krd.mini_project1.models.service;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class BasicInterceptorHelper implements Interceptor {

    private String credentials;

    public BasicInterceptorHelper(String user, String password) {
        this.credentials = Credentials.basic(user, password);
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Interceptor.Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder builder = original.newBuilder()
                .addHeader("Authorization", "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=");
//                .header("Authorization", credentials);


        Request request = builder.build();
        return chain.proceed(request);
    }
}
